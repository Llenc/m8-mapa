package com.example.incivisme.ui.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.incivisme.Incidencia;
import com.example.incivisme.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class LlistarFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_llistar, container, false);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance("https://incivisme-f99bc-default-rtdb.europe-west1.firebasedatabase.app").getReference();

        DatabaseReference users = base.child("users");
        String userId = auth.getUid();
        if (userId == null) {

        }
        else {
            DatabaseReference uid = users.child(userId);
            DatabaseReference incidencies = uid.child("incidencies");


            FirebaseListOptions<Incidencia> options = new FirebaseListOptions.Builder<Incidencia>()
                    .setQuery(incidencies, Incidencia.class)
                    .setLayout(R.layout.itemslistview)
                    .setLifecycleOwner(this)
                    .build();


            FirebaseListAdapter<Incidencia> adapter = new FirebaseListAdapter<Incidencia>(options) {
                @Override
                protected void populateView(View v, Incidencia model, int position) {
                    TextView txtDescripcio = v.findViewById(R.id.txtDescripcio);
                    TextView txtAdreca = v.findViewById(R.id.txtAdreca);

                    txtDescripcio.setText(model.getProblema());
                    txtAdreca.setText(model.getDireccio());
                }
            };

            ListView lvIncidencies = view.findViewById(R.id.lvIncidencies);
            lvIncidencies.setAdapter(adapter);



        }
        return view;
    }
}