package com.example.incivisme.ui.notifications;

import com.example.incivisme.Incidencia;
import com.example.incivisme.R;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.bumptech.glide.Glide;
import com.example.incivisme.SharedViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class NotificarFragment extends Fragment {
    ProgressBar mLoading;
    private TextInputEditText txtLatitud;
    private TextInputEditText txtLongitud;
    private TextInputEditText txtDireccio;
    private TextInputEditText txtDescripcio;
    String mCurrentPhotoPath;
    private Uri photoURI;
    static final int REQUEST_TAKE_PHOTO = 1;
    private ImageView Foto;
    private String downloadUrl;

    private Button buttonNotificar;
    private SharedViewModel model;

    public NotificarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificar, container, false);

        Foto = view.findViewById(R.id.foto);
        mLoading = view.findViewById(R.id.loading);

        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        txtLatitud = view.findViewById(R.id.txtLatitud);
        txtLongitud = view.findViewById(R.id.txtLongitud);
        txtDireccio = view.findViewById(R.id.txtDireccio);
        txtDescripcio = view.findViewById(R.id.txtDescripcio);
        buttonNotificar = view.findViewById(R.id.button_notificar);

        Button buttonFoto = view.findViewById(R.id.button_foto);

        model.getCurrentAddress().observe(getViewLifecycleOwner(), address -> {
            txtDireccio.setText(getString(R.string.address_text,
                    address, System.currentTimeMillis()));
        });
        model.getCurrentLatLng().observe(getViewLifecycleOwner(), latlng -> {
            txtLatitud.setText(String.valueOf(latlng.latitude));
            txtLongitud.setText(String.valueOf(latlng.longitude));
        });

        model.getProgressBar().observe(getViewLifecycleOwner(), visible -> {
            if (visible)
                mLoading.setVisibility(ProgressBar.VISIBLE);
            else
                mLoading.setVisibility(ProgressBar.INVISIBLE);
        });

        model.switchTrackingLocation();

        buttonNotificar.setOnClickListener(button -> {

            FirebaseStorage storage = FirebaseStorage.getInstance("gs://incivisme-f99bc.appspot.com");
            StorageReference storageRef = storage.getReference();

            StorageReference imageRef = storageRef.child(mCurrentPhotoPath);
            UploadTask uploadTask = imageRef.putFile(photoURI);

            uploadTask.addOnSuccessListener(taskSnapshot -> {
                taskSnapshot.getMetadata();

                imageRef.getDownloadUrl().addOnCompleteListener(task -> {
                    Uri downloadUri = task.getResult();
                    Glide.with(this).load(downloadUri).into(Foto);

                    downloadUrl = downloadUri.toString();

                    Incidencia incidencia = new Incidencia();
                    incidencia.setDireccio(txtDireccio.getText().toString());
                    incidencia.setLatitud(txtLatitud.getText().toString());
                    incidencia.setLongitud(txtLongitud.getText().toString());
                    incidencia.setProblema(txtDescripcio.getText().toString());
                    incidencia.setUrl(downloadUrl);

                    Log.d("BBB",incidencia.toString());

                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    DatabaseReference base = FirebaseDatabase.getInstance("https://incivisme-f99bc-default-rtdb.europe-west1.firebasedatabase.app").getReference();

                    DatabaseReference users = base.child("users");
                    DatabaseReference uid = users.child(auth.getUid());
                    DatabaseReference incidencies = uid.child("incidencies");

                    DatabaseReference reference = incidencies.push();
                    reference.setValue(incidencia);

                    Toast.makeText(getContext(), "Avís donat", Toast.LENGTH_SHORT).show();


                });
            });
        });

        buttonFoto.setOnClickListener(button -> {
            dispatchTakePictureIntent();
        });

        return view;

    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String imageFileName = "JPEG" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.d("AAA", String.valueOf(takePictureIntent.resolveActivity(
                getContext().getPackageManager())));

        if (takePictureIntent.resolveActivity(
                getContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO) {
            if (resultCode == Activity.RESULT_OK) {
                Glide.with(this).load(photoURI).into(Foto);
            } else {
                Toast.makeText(getContext(),
                        "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}